<?php

require_once("../db_connect.php");
require("../function.php");

if ($_SERVER["REQUEST_METHOD"] == "GET") $method = $_GET;
else $method = $_GET;

switch ($method["choice"]) {
    case 'select_id':
        if (isset($_GET["id"])) {
        
        $req = $db->prepare("SELECT * FROM product WHERE id = ?"); 
        $req->execute([$_GET["id"]]);
        $article = $req->fetch(PDO::FETCH_ASSOC);
        echo json_encode(["success" => true, "article" => $article]);
    } else { 
        echo json_encode(["success" => false, "error" => "Erreur lors de la sélection de l'article"]);
    }
    break;
}