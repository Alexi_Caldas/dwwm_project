<?php

require_once("../db_connect.php");

if(isset($_POST['envoyer'])) {
    if (
        empty(trim($_POST["lastname"])) ||
        empty(trim($_POST["firstname"])) ||
        empty(trim($_POST["email"])) ||
        empty(trim($_POST["message"]))
    ) {
        echo json_encode(["success" => false, "error" => "donnée manquante sur une ou plusieurs cases"]);
        die;}
            
        $req = $db->prepare("INSERT INTO contact (lastname, firstname, email, `message`) VALUES (:lastname, :firstname, :email, :message)");

        $req->bindValue(":lastname", $_POST["lastname"]);
        $req->bindValue(":firstname", $_POST["firstname"]);
        $req->bindValue(":email", $_POST["email"]);
        $req->bindValue(":message", $_POST["message"]);
        $req->execute();
    }
            
