CREATE TABLE supplier (
    id INT(11) auto_increment NOT NULL,
    name VARCHAR(45) NOT NULL,
    country VARCHAR(45) NOT NULL,
    num_str INT(11) NOT NULL,
    nom_str VARCHAR(100) NOT NULL,
    zipcode INT(11) NOT NULL,
    town VARCHAR (45) NOT NULL,
    PRIMARY KEY (id)
);

CREATE TABLE category (
    id INT(11) auto_increment NOT NULL,
    name_category VARCHAR(45) NOT NULL,
    PRIMARY KEY (id)
);

CREATE TABLE product (
    id INT(11) AUTO_INCREMENT NOT NULL,
    name VARCHAR(100) NOT NULL,
    reference VARCHAR(45) NOT NULL,
    price_t DECIMAL(10.0) NOT NULL,
    price_wt DECIMAL(10.0) NOT NULL,
    id_category INT(11) NOT NULL,
    FOREIGN KEY (id_category) REFERENCES category(id),
    PRIMARY KEY (id)
);

CREATE TABLE user (
    id INT(11) auto_increment NOT NULL,
    lastname VARCHAR(45) NOT NULL,
    firstname VARCHAR(45) NOT NULL,
    number_str INT(11) NOT NULL,    // "str=street"
    name_str VARCHAR(100) NOT NULL, 
    zipcode INT(11) NOT NULL,       
    town VARCHAR (45) NOT NULL,
    email VARCHAR(255) NOT NULL,
    phone_number DECIMAL(10.0) NOT NULL,
    identifiant VARCHAR(45) NOT NULL,
    pwd VARCHAR (75) NOT NULL,
    PRIMARY KEY (id)
);

CREATE TABLE order (
    id INT(11) auto_increment NOT NULL,
    number_str INT(11) NOT NULL,
    name_str VARCHAR(100) NOT NULL,
    zipcode INT(11) NOT NULL,
    town VARCHAR (45) NOT NULL,
    date_order DATETIME(45) NOT NULL,
    total_price INT(11) NOT NULL,
    FOREIGN KEY (id_user) REFERENCES user(id),
    PRIMARY KEY (id)
);

CREATE TABLE supply_product ( 
    id_supplier INT(11) NOT NULL,
    id_product INT(11) NOT NULL,
    PRIMARY KEY(id_product, id_supplier),
    FOREIGN KEY(id_product) REFERENCES product(id),
    FOREIGN KEY(id_supplier) REFERENCES supplier(id)
);

CREATE TABLE order_product ( 
    id_order INT(11) NOT NULL,
    id_product INT(11) NOT NULL,
    PRIMARY KEY(id_product, id_order),
    FOREIGN KEY(id_product) REFERENCES product(id),
    FOREIGN KEY(id_order) REFERENCES order(id)
);



CREATE TABLE help (
    id INT(11) AUTO_INCREMENT NOT NULL,
    lastname VARCHAR(45) NOT NULL,
    firstname VARCHAR(45) NOT NULL,
    email VARCHAR(255) NOT NULL,
    `message`VARCHAR(255) NOT NULL,
    PRIMARY KEY (id)
);

CREATE TABLE showroom (
    id INT(11) AUTO_INCREMENT NOT NULL,
    name VARCHAR(100) NOT NULL,
    reference VARCHAR(45) NOT NULL,
    price_t DECIMAL(10.0) NOT NULL,
    price_wt DECIMAL(10.0) NOT NULL,
    PRIMARY KEY (id)
);